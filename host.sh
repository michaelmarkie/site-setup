#!/bin/bash


read -p "Type the domain you wish to use, followed by [ENTER]: " domain

# Whatever happens we need a location to use for certbot
mkdir /var/www/$domain

read -n 1 -p "Do you wish to setup reverse proxy? (y/n)? " reverse

if [ $reverse = "n" ] ; then

        echo ""
        echo -n "GIT repository URL to clone? followed by [ENTER]: (optional) "
        read git
        git=${git:-false}


        if [ $git != false ] ; then
                read -e -p "GIT Branch, followed by [ENTER]: " -i "master" branch

                read -e -p "Working directory, followed by [ENTER]: " -i "dist" dir

                echo "Cloning GIT..."
                git clone --recursive -b $branch $git /var/www/$domain
        fi

else

    	read -p "Please specify URL of reverse proxy, followed by [ENTER]: " proxy

fi


# We'll setup a http location so as to setup SSL without any issues.
# Later we'll switch it to https
sed "s/DOMAIN_NAME/$domain/g" .host.http > .host.tmp
mv .host.tmp /etc/nginx/sites-available/$domain
ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/$domain
sudo service nginx restart

echo "Running certbot..."
sudo certbot certonly --webroot -w /var/www/$domain/ -d $domain
##################################################################





if [ $reverse = "n" ] ; then

        echo "Replacing variables..."
        sed "s/DOMAIN_NAME/$domain/g" .host.ssl > .host.tmp2
        sed "s/WORKING_DIR/$dir/g" .host.tmp2 > .host.tmp

else

    	echo "Replacing variables..."
        sed "s/DOMAIN_NAME/$domain/g" .host.proxy > .host.tmp2
        sed "s/PROXY_URL/$proxy/g" .host.tmp2 > .host.tmp

fi



echo "Moving file to nginx..."
sudo cp .host.tmp /etc/nginx/sites-available/$domain

echo "Restarting nginx..."
sudo service nginx restart

echo "Deleting generated files..."
rm .host.tmp2 .host.tmp

echo ""
echo -e "\033[1mCONGRATULATIONS!\033[0m"
echo "https://$domain has been setup."
